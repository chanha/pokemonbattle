﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    enum AttackCategory
    {
        Physical, Special, Other
    }

    abstract class PokemonMove
    {
        // pp는 Power Point의 줄임으로 기술을 몇 번 사용할 수 있는지를 알려준다
        protected int MaxPP;
        protected int currentPP;

        public abstract AttackCategory GetAttackCategory();
        public abstract int GetPower();

        public abstract PokemonType GetAttackType();

        public bool IsSamePokemonType(List<PokemonType> types)
        {
            foreach (PokemonType type in types)
                if (type == GetAttackType())
                    return true;
            return false;
        }

    }
}
