﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class Tackle : PokemonMove
    {
        private static string name;
        private static PokemonType type;
        private static int power;
        private static int accuracy;
        private static AttackCategory attackCategory;
        // Priority는 우선도라고 하며 이 수치가 높을 수록 먼저 행동할 수 있습니다.
        private static int priority;

        static Tackle()
        {
            name = "Tackle";
            type = PokemonType.Normal;
            power = 40;
            accuracy = 100;
            attackCategory = AttackCategory.Physical;
            priority = 0;
        }

        public Tackle()
        {
            MaxPP = 35;
            currentPP = MaxPP;
        }

        public override PokemonType GetAttackType()
        {
            return type;
        }

        public override AttackCategory GetAttackCategory()
        {
            return attackCategory;
        }

        public override int GetPower()
        {
            return power;
        }

    }
}
