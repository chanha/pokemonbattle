﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    static class AttackEffectivenessChecker
    {
        static PokemonType effectiveWhenUsingBug = PokemonType.Dark | PokemonType.Psychic | PokemonType.Grass;
        static PokemonType effectiveWhenUsingDark = PokemonType.Ghost | PokemonType.Psychic;
        static PokemonType effectiveWhenUsingDragon = PokemonType.Dragon;
        static PokemonType effectiveWhenUsingElectric = PokemonType.Water | PokemonType.Flying;
        static PokemonType effectiveWhenUsingFairy = PokemonType.Fight | PokemonType.Dragon | PokemonType.Dark;
        static PokemonType effectiveWhenUsingFight = PokemonType.Steel | PokemonType.Normal | PokemonType.Rock | PokemonType.Dark | PokemonType.Ice;
        static PokemonType effectiveWhenUsingFire = PokemonType.Steel | PokemonType.Bug | PokemonType.Ice | PokemonType.Grass;
        static PokemonType effectiveWhenUsingFlying = PokemonType.Fight | PokemonType.Bug | PokemonType.Grass;
        static PokemonType effectiveWhenUsingGhost = PokemonType.Ghost | PokemonType.Psychic;
        static PokemonType effectiveWhenUsingGrass = PokemonType.Ground | PokemonType.Water | PokemonType.Rock;
        static PokemonType effectiveWhenUsingGround = PokemonType.Steel | PokemonType.Poison | PokemonType.Rock | PokemonType.Fire | PokemonType.Electric;
        static PokemonType effectiveWhenUsingIce = PokemonType.Dragon | PokemonType.Ground | PokemonType.Flying | PokemonType.Grass;
        static PokemonType effectiveWhenUsingNormal = PokemonType.NULL;
        static PokemonType effectiveWhenUsingPoison = PokemonType.Grass | PokemonType.Fairy;
        static PokemonType effectiveWhenUsingPsychic = PokemonType.Fight | PokemonType.Poison;
        static PokemonType effectiveWhenUsingRock = PokemonType.Bug | PokemonType.Fire | PokemonType.Flying | PokemonType.Ice;
        static PokemonType effectiveWhenUsingSteel = PokemonType.Rock | PokemonType.Ice | PokemonType.Fairy;
        static PokemonType effectiveWhenUsingWater = PokemonType.Fire | PokemonType.Ground | PokemonType.Rock;

        static PokemonType weaknessWhenUsingBug = PokemonType.Steel | PokemonType.Fight | PokemonType.Ghost | PokemonType.Poison | PokemonType.Fire | PokemonType.Flying | PokemonType.Fairy;
        static PokemonType weaknessWhenUsingDark = PokemonType.Fight | PokemonType.Dark | PokemonType.Fairy;
        static PokemonType weaknessWhenUsingDragon = PokemonType.Steel;
        static PokemonType weaknessWhenUsingElectric = PokemonType.Dragon | PokemonType.Electric | PokemonType.Grass;
        static PokemonType weaknessWhenUsingFairy = PokemonType.Steel | PokemonType.Poison | PokemonType.Dark;
        static PokemonType weaknessWhenUsingFight = PokemonType.Poison | PokemonType.Bug | PokemonType.Flying | PokemonType.Psychic | PokemonType.Fairy;
        static PokemonType weaknessWhenUsingFire = PokemonType.Dragon | PokemonType.Water | PokemonType.Rock | PokemonType.Fire;
        static PokemonType weaknessWhenUsingFlying = PokemonType.Steel | PokemonType.Rock | PokemonType.Electric;
        static PokemonType weaknessWhenUsingGhost = PokemonType.Dark;
        static PokemonType weaknessWhenUsingGrass = PokemonType.Steel | PokemonType.Poison | PokemonType.Dragon | PokemonType.Bug | PokemonType.Fire | PokemonType.Flying | PokemonType.Grass;
        static PokemonType weaknessWhenUsingGround = PokemonType.Bug | PokemonType.Grass;
        static PokemonType weaknessWhenUsingIce = PokemonType.Steel | PokemonType.Water | PokemonType.Fire | PokemonType.Ice;
        static PokemonType weaknessWhenUsingNormal = PokemonType.Steel | PokemonType.Rock;
        static PokemonType weaknessWhenUsingPoison = PokemonType.Ghost | PokemonType.Poison | PokemonType.Ground | PokemonType.Rock;
        static PokemonType weaknessWhenUsingPsychic = PokemonType.Steel | PokemonType.Psychic;
        static PokemonType weaknessWhenUsingRock = PokemonType.Steel | PokemonType.Fight | PokemonType.Ground;
        static PokemonType weaknessWhenUsingSteel = PokemonType.Steel | PokemonType.Water | PokemonType.Fire | PokemonType.Electric;
        static PokemonType weaknessWhenUsingWater = PokemonType.Dragon | PokemonType.Water | PokemonType.Grass;
        
        // 공격 무효화
        static PokemonType hasInvalidityAttackTypes = PokemonType.Dragon | PokemonType.Electric | PokemonType.Fight
                                                    | PokemonType.Ghost | PokemonType.Ground | PokemonType.Normal
                                                    | PokemonType.Poison | PokemonType.Psychic;

        static PokemonType invalidWhenUsingBug = PokemonType.NULL;
        static PokemonType invalidWhenUsingDark = PokemonType.NULL;
        static PokemonType invalidWhenUsingDragon = PokemonType.Fairy;
        static PokemonType invalidWhenUsingElectric = PokemonType.Ground;
        static PokemonType invalidWhenUsingFairy = PokemonType.NULL;
        static PokemonType invalidWhenUsingFight = PokemonType.Ghost;
        static PokemonType invalidWhenUsingFire = PokemonType.NULL;
        static PokemonType invalidWhenUsingFlying = PokemonType.NULL;
        static PokemonType invalidWhenUsingGhost = PokemonType.Normal;
        static PokemonType invalidWhenUsingGrass = PokemonType.NULL;
        static PokemonType invalidWhenUsingGround = PokemonType.Flying;
        static PokemonType invalidWhenUsingIce = PokemonType.NULL;
        static PokemonType invalidWhenUsingNormal = PokemonType.Ghost;
        static PokemonType invalidWhenUsingPoison = PokemonType.Steel;
        static PokemonType invalidWhenUsingPsychic = PokemonType.Dark;
        static PokemonType invalidWhenUsingRock = PokemonType.NULL;
        static PokemonType invalidWhenUsingSteel = PokemonType.NULL;
        static PokemonType invalidWhenUsingWater = PokemonType.NULL;

        // 무효화 되는지 체크
        static public bool IsInvalid(PokemonType attackType, List<PokemonType> defenceTypes)
        {
            if ((hasInvalidityAttackTypes & attackType) == attackType)
            {
                switch (attackType)
                {
                    case PokemonType.Dragon:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingDragon) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Electric:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingElectric) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Fight:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingFight) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Ghost:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingGhost) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Ground:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingGround) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Normal:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingNormal) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Poison:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingPoison) == pokemonType)
                                return true;
                        return false;
                    case PokemonType.Psychic:
                        foreach (PokemonType pokemonType in defenceTypes)
                            if ((pokemonType & invalidWhenUsingPsychic) == pokemonType)
                                return true;
                        return false;
                    default:
                        return false;
                }
            }
            else
                return false;
        }
        // 타입에 따라 배수 반환
        static public float EffectivenessAndWeaknessCheck(PokemonType attackType, List<PokemonType> defenceTypes)
        {
            float multiplier = 1;
            switch (attackType)
            {
                case PokemonType.Bug:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingBug & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingBug & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Dark:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingDark & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingDark & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Dragon:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingDragon & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingDragon & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Electric:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingElectric & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingElectric & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Fairy:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingFairy & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingFairy & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Fight:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingFight & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingFight & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Fire:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingFire & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingFire & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Flying:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingFlying & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingFlying & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Ghost:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingGhost & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingGhost & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Grass:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingGrass & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingGrass & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Ground:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingGround & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingGround & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Ice:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingIce & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingIce & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Normal:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingNormal & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingNormal & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Poison:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingPoison & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingPoison & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Psychic:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingPsychic & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingPsychic & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Rock:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingRock & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingRock & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Steel:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingSteel & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingSteel & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;

                case PokemonType.Water:
                    foreach (PokemonType pokemonType in defenceTypes)
                    {
                        if ((effectiveWhenUsingWater & pokemonType) == pokemonType)
                            multiplier *= Constant.EffectivenessAttackMultiplier;
                        else if ((weaknessWhenUsingWater & pokemonType) == pokemonType)
                            multiplier /= Constant.WeaknessAttackMultiplier;
                    }
                    return multiplier;
            }
            return multiplier;
        }


    }

}


