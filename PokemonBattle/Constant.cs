﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class Constant
    {
        public const int MaxNumberOfMoves = 4;
        public const int MaxLevel = 100;

        //Stat 관련
        public const int MaxIVs = 31;
        public const int MaxEVs = 252;
        public const float BenefitNatureValue = 1.1f;
        public const float HinderingNatureValue = 0.9f;

        //battle 관련
        public const int BaseStatMultiplier = 2;
        public const int EffortValueDivider = 4;
        public const int NumberOfNatures = 25;
        public const float CriticalDamage = 1.5f;
        public const float SameTypeMultiplier = 1.5f;
        public const int EffectivenessAttackMultiplier = 2;
        public const int WeaknessAttackMultiplier = 2;
        public const int MinWeatherTurn = 3;
        public const int MaxWeatherTurn = 5;
        public const int AttackRandomMin = 85;
        public const int AttackRandomMax = 100;

    }
}
