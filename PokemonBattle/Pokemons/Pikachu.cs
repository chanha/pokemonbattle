﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class Pikachu : Pokemon
    {
        //todo: 각각 포켓몬에 static으로 선언된 이 4가지 field값을 하나의 class 혹은 struct로 묶을 수는 없을까?
        static string realName;
        static BaseStat baseStat;
        /*
          todo: 타입을 Enum변수 하나로 해서 PokemonType의 Attribute를 Flags로 해놓고 하나의 변수에 2개의 타입 다 넣기 가능
          그런데 이렇게 할 경우는 메인 타입이 뭔지 구분을 할 수 없다는 단점이 있음(물론 계산시 메인과 서브를 따지지는 않지만)
          어떻게 할 지는 고민 해봐야겠지만 일단은 enum변수값을 2의 배수로 다 바꿔볼까
          enum을 Flags로하면 상성체크할때 굉장히 편하긴 할 것 같다.
         */
        static List<PokemonType> types = new List<PokemonType>();
        
        static Pikachu()
        {
            realName = "Pikachu";
            baseStat = new BaseStat();
            baseStat.SetBaseStat(35, 55, 40, 50, 50, 90);
            types.Add(PokemonType.Electric);
        }

        public Pikachu()
        {
            Random random = new Random();

            EVs = new EffortValue();
            IVs = new IndividualValue();

            level = 50;
            name = realName;
            gender = (random.Next(2) == 1) ? PokemonGender.Male : PokemonGender.Female;
            nature = (PokemonNature)random.Next(Constant.NumberOfNatures);
            
            hasHP = true;
            moves = new PokemonMove[Constant.MaxNumberOfMoves];

            EVs.SetEVs();
            IVs.SetIVs();

            SetRealStat(baseStat, EVs, IVs);

            currentHp = realStat.hp;
        }

        public override List<PokemonType> GetPokemonType()
        {
            return types;
        }

        public override void Faint()
        {
            FaintingEventArgs args = new FaintingEventArgs(realStat.hp, currentHp, 1, 1);
            OnFainting(args);
            if(args.Cancel)
            {
                currentHp = 1;
                return;
            }
            OnFainted(new FaintedEventArgs());
        }
    }
}
