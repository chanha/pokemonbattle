﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class Eevee : Pokemon
    {

        static string realName;
        static BaseStat baseStat;
        static List<PokemonType> types = new List<PokemonType>();

        static Eevee()
        {
            realName = "Eevee";
            baseStat = new BaseStat();
            baseStat.SetBaseStat(55, 55, 50, 45, 65, 55);
            types.Add(PokemonType.Normal);
        }

        public Eevee()
        {
            Random random = new Random();

            EVs = new EffortValue();
            IVs = new IndividualValue();

            level = 50;
            name = realName;
            gender = (random.Next(2) == 1) ? PokemonGender.Male : PokemonGender.Female;
            nature = (PokemonNature)random.Next(Constant.NumberOfNatures);

            hasHP = true;
            moves = new PokemonMove[Constant.MaxNumberOfMoves];

            EVs.SetEVs();
            IVs.SetIVs();

            SetRealStat(baseStat, EVs, IVs);

            currentHp = realStat.hp;
        }

        public override List<PokemonType> GetPokemonType()
        {
            return types;
        }

        public override void Faint()
        {
            Console.WriteLine("Evv.v.vv.v.");
        }
    }
}
