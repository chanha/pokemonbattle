﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class EffortValue
    {
        public int hp;
        public int attack;
        public int defence;
        public int spAttack;
        public int spDefence;
        public int speed;

        public void SetEVs()
        {
            Random random = new Random();

            hp = random.Next(1, Constant.MaxEVs + 1);
            attack = random.Next(1, Constant.MaxEVs + 1);
            defence = random.Next(1, Constant.MaxEVs + 1);
            spAttack = random.Next(1, Constant.MaxEVs + 1);
            spDefence = random.Next(1, Constant.MaxEVs + 1);
            speed = random.Next(1, Constant.MaxEVs + 1);
        }
    }
}
