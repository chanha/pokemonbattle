﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    struct RealStat
    {
        public int hp;
        public int attack;
        public int defence;
        public int spAttack;
        public int spDefence;
        public int speed;
    }
}
