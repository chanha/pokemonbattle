﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    /// <summary>
    /// PokemonNature를 매개변수로 받아 그 Nature가 
    /// 어떤 Stat을 올려주는 그룹인지 물어보고 bool값을 return 하는 함수들이 있다.
    /// </summary>
    static class PokemonNatures
    {
        public static bool IsAAttackBenefit(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Lonely:
                case PokemonNature.Adamant:
                case PokemonNature.Naughty:
                case PokemonNature.Brave:
                    return true;
            }
            return false;
        }
        public static bool IsADefenceBenefit(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Bold:
                case PokemonNature.Impish:
                case PokemonNature.Lax:
                case PokemonNature.Relaxed:
                    return true;
            }
            return false;
        }
        public static bool IsASpAttackBenefit(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Modest:
                case PokemonNature.Mild:
                case PokemonNature.Rash:
                case PokemonNature.Quiet:
                    return true;
            }
            return false;
        }
        public static bool IsASpDefenceBenefit(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Calm:
                case PokemonNature.Gentie:
                case PokemonNature.Careful:
                case PokemonNature.Sassy:
                    return true;
            }
            return false;
        }
        public static bool IsASpeedBenefit(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Timid:
                case PokemonNature.Hasty:
                case PokemonNature.Jolly:
                case PokemonNature.Naive:
                    return true;
            }
            return false;
        }

        public static bool IsAAttackHindering(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Bold:
                case PokemonNature.Modest:
                case PokemonNature.Calm:
                case PokemonNature.Timid:
                    return true;
            }
            return false;
        }
        public static bool IsADefenceHindering(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Lonely:
                case PokemonNature.Mild:
                case PokemonNature.Gentie:
                case PokemonNature.Hasty:
                    return true;
            }
            return false;
        }
        public static bool IsASpAttackHindering(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Adamant:
                case PokemonNature.Impish:
                case PokemonNature.Careful:
                case PokemonNature.Jolly:
                    return true;
            }
            return false;
        }
        public static bool IsASpDefenceHindering(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Naughty:
                case PokemonNature.Lax:
                case PokemonNature.Rash:
                case PokemonNature.Naive:
                    return true;
            }
            return false;
        }
        public static bool IsASpeedHindering(PokemonNature PN)
        {
            switch (PN)
            {
                case PokemonNature.Brave:
                case PokemonNature.Relaxed:
                case PokemonNature.Quiet:
                case PokemonNature.Sassy:
                    return true;
            }
            return false;
        }

    }
}
