﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{

    /// <summary>
    ///(Natures Name)(Raises)(Lowers) :
    ///Hardy
    ///がんばりや   None None
    ///Lonely
    ///さみしがり   Attack Defense
    ///Brave
    ///ゆうかん     Attack Speed 
    ///Adamant
    ///いじっぱり   Attack Special
    ///Naughty
    ///やんちゃ     Attack Special Defense
    ///Bold
    ///ずぶとい     Defense Attack
    ///Docile
    ///すなお       None None
    ///Relaxed
    ///のんき       Defense Speed 
    ///Impish
    ///わんぱく     Defense Special Attack
    ///Lax
    ///のうてんき   Defense Special Defense
    ///Timid
    ///おくびょう   Speed Attack
    ///Hasty
    ///せっかち     Speed Defense
    ///Serious
    ///まじめ       None None 
    ///Jolly
    ///ようき       Speed Special Attack
    ///Naïve
    ///むじゃき     Speed Special Defense
    ///Modest
    ///ひかえめ     Special Attack  Attack
    ///Mild
    ///おっとり     Special Attack  Defense
    ///Quiet
    ///れいせい     Special Attack  Speed
    ///Bashful
    ///てれや       None None
    ///Rash
    ///うっかりや   Special Attack  Special Defense
    ///Calm
    ///おだやか     Special Defense Attack
    ///Gentle
    ///おとなしい   Special Defense Defense
    ///Sassy
    ///なまいき     Special Defense Speed
    ///Careful
    ///しんちょう   Special Defense Special Attack 
    ///Quirky
    ///きまぐれ     None None
    /// </summary>
    enum PokemonNature
    {
        Hardy,
        Lonely,
        Brave,
        Adamant,
        Naughty,
        Bold,
        Docile,
        Relaxed,
        Impish,
        Lax,
        Timid,
        Hasty,
        Serious,
        Jolly,
        Naive,
        Modest,
        Mild,
        Quiet,
        Bashful,
        Rash,
        Calm,
        Gentie,
        Sassy,
        Careful,
        Quirky
    }
}
