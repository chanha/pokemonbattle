﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class BaseStat
    {
        public int hp;
        public int attack;
        public int defence;
        public int spAttack;
        public int spDefence;
        public int speed;

        public void SetBaseStat(int hp, int attack, int defence, int spAttack, int spDefence, int speed)
        {
            this.hp = hp;
            this.attack = attack;
            this.defence = defence;
            this.spAttack = spAttack;
            this.spDefence = spDefence;
            this.speed = speed;
        }
    }
}
