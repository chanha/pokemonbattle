﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    //todo: 특성을 추가해야함
    //todo: 상태이상을 추가해야함

    abstract class Pokemon
    {
        // 종족값은 각각 클래스에 static field로 선언한다.
        // 포켓몬의 진짜 이름 역시 각각 클래스에 static field로 선언한다.
        // 이름, 노력치, 개체값, 능력치는 Pokemon 클래스에 protected로 선언한다.
        // PokemonMove는 최대 4개까지 가질 수 있다

        protected string name;
        protected PokemonGender gender;
        protected PokemonNature nature;

        // 실제 능력치로 BaseStat, EffortValue, IndividualValue의 연산으로 정해진다
        protected int level;

        protected RealStat realStat;
        protected int currentHp;

        protected bool hasHP;
        protected PokemonMove[] moves;

        protected EffortValue EVs;
        protected IndividualValue IVs;

        #region Event Fainted
        public event EventHandler<FaintedEventArgs> Fainted;

        protected void OnFainted(FaintedEventArgs e)
        {
            if (Fainted != null)
                Fainted(this, e);
        }

        private FaintedEventArgs OnFainted()
        {
            FaintedEventArgs args = new FaintedEventArgs();
            OnFainted(args);
            return args;
        }

        public class FaintedEventArgs : EventArgs
        {
        }
        #endregion

        #region Event Fainting
        public event EventHandler<FaintingEventArgs> Fainting;

        protected virtual void OnFainting(FaintingEventArgs e)
        {
            if (Fainting != null)
                Fainting(this, e);
        }

        private FaintingEventArgs OnFainting(int maxHp, int currentHp, int item, int ability)
        {
            FaintingEventArgs args = new FaintingEventArgs(maxHp, currentHp, item, ability);
            OnFainting(args);

            return args;
        }

        public class FaintingEventArgs : EventArgs
        {
            public int MaxHp { get; set; }
            public int CurrentHp { get; set; }
            // todo : 나중에 Item이랑 Ability는 enum으로 만들것 같다.
            public int Item { get; set; }
            public int Ability { get; set; }
            public bool Cancel { get; set; }

            public FaintingEventArgs(int maxHp, int currentHp, int item, int ability)
            {
                MaxHp = maxHp;
                CurrentHp = currentHp;
                Item = item;
                Ability = ability;
            }
        }
        #endregion

        public bool IsAlive() { return hasHP; }

        // todo : 이게 여기에 있는게 맞을 까?
        protected int PowerUpRank { get; set; }

        // todo: UsePokemonMove는 아직 미완이고 추가할 것도 바꿀 것도 많음
        public void UsePokemonMove(PokemonMove pokemonMove, Pokemon target)
        {
            Random random = new Random();
            int damage = 0;
            float typeEffectiveMultiplier = 1;
            float critical = 1;
            float sameType = 1;
            float mod1 = 1;
            float mod2 = 1;
            float mod3 = 1;

            if (pokemonMove.IsSamePokemonType(GetPokemonType()))
            {
                sameType = Constant.SameTypeMultiplier;
                if (false)
                {
                    // todo : 특성 적응력에 대한 배수 처리해야하는 부분
                    sameType = 2.0f;
                }
            }

            PokemonType attackType = pokemonMove.GetAttackType();
            List<PokemonType> defenceTypes = target.GetPokemonType();

            if (AttackEffectivenessChecker.IsInvalid(attackType, defenceTypes))
            {
                Console.WriteLine("효과가 없다.");
                return;
            }

            typeEffectiveMultiplier = AttackEffectivenessChecker.EffectivenessAndWeaknessCheck(attackType, defenceTypes);

            if(typeEffectiveMultiplier >= 2)
                Console.WriteLine("효과는 굉장했다.");
            if(typeEffectiveMultiplier < 1)
                Console.WriteLine("효과가 부족한것 같다.");

            /*
             (데미지 = (((((((레벨 × 2 ÷ 5) 
                             + 2)
                             × 위력 × 특수공격 ÷ 50)
                             ÷ 특수방어)
                             × Mod1)
                             + 2)
                             × [[급소]] × Mod2 ×  랜덤수 ÷ 100)
                             × 자속보정 × 타입상성1 × 타입상성2 × Mod3)
             */

            // todo : 위력 계산 함수
            // (위력 = [[도우미]] × 기술위력 × 도구보정 × [[충전]] × [[흙놀이]] × [[물놀이]] × 특성보정 × 상대특성보정)
            // todo : 특수공격, 특수방어 계산 함수
            // (특수공격 = 스탯 × [[랭크 (상태변화)|랭크]]보정 × [[특성]]보정 × [[도구]]보정)
            // (특수방어 = 스탯 × [[랭크]]보정 × Mod × 자폭보정)
            // todo : Mod1, Mod2, Mod3 계산 함수
            // (Mod1 = [[화상]] × [[리플렉터]]·[[빛의장막]] × [[더블배틀]] × [[날씨]] × [[타오르는불꽃]])
            //  Mod2 = 생명의구슬을 장착하면 1.3, 메트로놈을 장착하고 같은 기술을 써나가면 1, 1.1, 1.2, 1.3, ..., 2, 그 외는 1
            // (Mod3 = [[하드록]]·[[필터]] × [[달인의띠]] × [[색안경]] × 타입별위력반감[[열매]])


            switch (pokemonMove.GetAttackCategory())
            {
                case AttackCategory.Physical:

                    damage = (int)(((((((((level * 2 / 5)
                                           + 2)
                                           * pokemonMove.GetPower() * realStat.attack / 50)
                                           / target.realStat.defence)
                                           * mod1)
                                           + 2)
                                           * critical * mod2 * (random.Next(Constant.AttackRandomMin,Constant.AttackRandomMax + 1) / 100))
                                           * sameType * typeEffectiveMultiplier * mod3));

                    Console.WriteLine(damage + "의 데미지");
                    target.Damaged(damage);
                    break;
                case AttackCategory.Special:
                    damage = (int)((((((((level * 2 / 5) + 2) * pokemonMove.GetPower() * realStat.spAttack / 50) / target.realStat.spDefence) * mod1) + 2) * critical * mod2 * (100 / 100)) * sameType * typeEffectiveMultiplier * mod3);
                    Console.WriteLine(damage + "의 데미지");
                    target.Damaged(damage);
                    break;
                case AttackCategory.Other:

                    break;
            }

        }

        public PokemonMove SelectMove(int index)
        {
            return moves[index];
        }

        public void AddMoves(PokemonMove pokemonMove, int index)
        {
            moves[index] = pokemonMove;
        }
        
        public void SetRealStat(BaseStat BS, EffortValue effortValues, IndividualValue individualValues)
        {
            realStat.hp = (int)(((BS.hp * Constant.BaseStatMultiplier) + individualValues.hp + (effortValues.hp / Constant.EffortValueDivider)) * ((float)level / Constant.MaxLevel) + 10 + level);
            realStat.attack = ((BS.attack * Constant.BaseStatMultiplier + individualValues.attack + effortValues.attack / Constant.EffortValueDivider) / 2 + 5);
            realStat.defence = ((BS.defence * Constant.BaseStatMultiplier + individualValues.defence + effortValues.defence / Constant.EffortValueDivider) / 2 + 5);
            realStat.spAttack = ((BS.spAttack * Constant.BaseStatMultiplier + individualValues.spAttack + effortValues.spAttack / Constant.EffortValueDivider) / 2 + 5);
            realStat.spDefence = ((BS.spDefence * Constant.BaseStatMultiplier + individualValues.spDefence + effortValues.spDefence / Constant.EffortValueDivider) / 2 + 5);
            realStat.speed = ((BS.speed * Constant.BaseStatMultiplier + individualValues.speed + effortValues.speed / Constant.EffortValueDivider) / 2 + 5);

            AdjustNatureToStat();
        }

        public void AdjustNatureToStat()
        {
            if (PokemonNatures.IsAAttackBenefit(nature))
                realStat.attack = (int)(realStat.attack * Constant.BenefitNatureValue);
            if (PokemonNatures.IsADefenceBenefit(nature))
                realStat.defence = (int)(realStat.defence * Constant.BenefitNatureValue);
            if (PokemonNatures.IsASpAttackBenefit(nature))
                realStat.spAttack = (int)(realStat.spAttack * Constant.BenefitNatureValue);
            if (PokemonNatures.IsASpDefenceBenefit(nature))
                realStat.spDefence = (int)(realStat.spDefence * Constant.BenefitNatureValue);
            if (PokemonNatures.IsASpeedBenefit(nature))
                realStat.speed = (int)(realStat.speed * Constant.BenefitNatureValue);

            if (PokemonNatures.IsAAttackHindering(nature))
                realStat.attack = (int)(realStat.attack * Constant.HinderingNatureValue);
            if (PokemonNatures.IsADefenceHindering(nature))
                realStat.defence = (int)(realStat.defence * Constant.HinderingNatureValue);
            if (PokemonNatures.IsASpAttackHindering(nature))
                realStat.spAttack = (int)(realStat.spAttack * Constant.HinderingNatureValue);
            if (PokemonNatures.IsASpDefenceHindering(nature))
                realStat.spDefence = (int)(realStat.spDefence * Constant.HinderingNatureValue);
            if (PokemonNatures.IsASpeedHindering(nature))
                realStat.speed = (int)(realStat.speed * Constant.HinderingNatureValue);
        }

        public void PrintStat()
        {
            Console.WriteLine("Name: " + name + "   Nature:" + nature);
            Console.Write("Type: ");
            foreach (PokemonType pokemonType in GetPokemonType())
            {
                Console.Write(pokemonType + " ");
            }
            Console.WriteLine();
            Console.WriteLine("Hp:         " + realStat.hp);
            Console.WriteLine("Attack:     " + realStat.attack);
            Console.WriteLine("Defence:    " + realStat.defence);
            Console.WriteLine("Sp.Attack   " + realStat.spAttack);
            Console.WriteLine("Sp.Defence: " + realStat.spDefence);
            Console.WriteLine("Speed:      " + realStat.speed);
            Console.WriteLine();
        }

        public void Damaged(int damage)
        {
            currentHp -= damage;
            if (currentHp <= 0)
            {
                currentHp = 0;
                hasHP = false;
            }
            Console.WriteLine(name + " Current HP: " + currentHp);
        }

        public abstract List<PokemonType> GetPokemonType();
        public abstract void Faint();




    }
}