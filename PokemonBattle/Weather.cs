﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    enum Weather
    {
        Normal,
        Sunshine,
        HarshSunshine,
        Rain,
        HeavyRain,
        Hail,
        Sandstorm,
        StrongWind,
        Fog,
        ShadowSky
    }

    interface IWeather
    {
        void ChangeWeather(Weather weather);
        void EndWeather(Weather weather);
        void WeatherEffect();
    }


}
