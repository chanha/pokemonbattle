﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBattle
{
    class Battlefield : IWeather
    {
        private Weather weather;
        private int weatherTurn;

        public void ChangeWeather(Weather weather)
        {
            Console.WriteLine("날씨가" + weather.ToString() +"로 바뀌었다.");
        }
        public void EndWeather(Weather weather)
        {
            if (weatherTurn == Constant.MaxWeatherTurn)
            {
                Console.WriteLine("날씨가 원래대로 돌아왔다.");
            }
            else if(weatherTurn >= Constant.MinWeatherTurn)
            {
                Console.WriteLine("날씨가 원래대로 돌아왔다.");
            }
        }
        public void WeatherEffect()
        {

        }

        public static float CalculateMod1()
        {
            // todo : function not implemented
            float mod1 = 1;

            return mod1;
        }
        public static float CalculateMod2()
        {
            // todo : function not implemented
            float mod2 = 1;

            return mod2;
        }
        public static float CalculateMod3()
        {
            // todo : function not implemented
            float mod3 = 1;

            return mod3;
        }
        public static float CalculateSpecialAttack()
        {
            // todo : function not implemented
            float specialAttack = 1;

            return specialAttack;
        }
        public static float CalculateSpecialDefence()
        {
            // todo : function not implemented
            float specialDefence = 1;

            return specialDefence;
        }

        #region Event Weather Changed
        public event EventHandler<WeatherChangedEventArgs> WeatherChanged;

        protected void OnWeatherChanged(WeatherChangedEventArgs e)
        {
            if (WeatherChanged != null)
                WeatherChanged(this, e);
        }

        private WeatherChangedEventArgs OnWeatherChanged(Weather newWeather)
        {
            WeatherChangedEventArgs args = new WeatherChangedEventArgs(newWeather);
            OnWeatherChanged(args);
            return args;
        }

        public class WeatherChangedEventArgs
        {
            public WeatherChangedEventArgs(Weather newWeather)
            {
                NewWeather = newWeather;
            }

            public Weather NewWeather { get; set; }
        }
        #endregion

        #region Event Weather Changing
        public event EventHandler<WeatherChangingEventArgs> WeatherChanging;

        protected void OnWeatherChanging(WeatherChangingEventArgs e)
        {
            if (WeatherChanging != null)
                WeatherChanging(this, e);
        }

        private WeatherChangingEventArgs OnWeatherChanging(Weather currentWeather, Weather newWeather)
        {
            WeatherChangingEventArgs args = new WeatherChangingEventArgs(currentWeather, newWeather);
            OnWeatherChanging(args);
            return args;
        }

        public class WeatherChangingEventArgs
        {
            public WeatherChangingEventArgs(Weather currentWeather, Weather newWeather)
            {
                CurrentWeather = currentWeather;
                NewWeather = newWeather;
            }

            public Weather CurrentWeather { get; set; }
            public Weather NewWeather { get; set; }
        }
        #endregion

    }
}
